package com.example.d_material;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Objects;

public class Select_branch_semester extends AppCompatActivity {
    Spinner spinner1;
    Spinner spinner2;
    Button save;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_branch_semester);
        Objects.requireNonNull(getSupportActionBar()).setTitle(Html.fromHtml("<font color=\"#173884\">" + getString(R.string.app_name)+"</font>"));
        spinner1=findViewById(R.id.Select_Branch);
        spinner2=findViewById(R.id.Select_Sem);

        ArrayList<String> branch=new ArrayList<String>();
        branch.add("Select Branch");
        branch.add("Computer Engineering");
        branch.add("Civil Engineering");
        branch.add("Mechanical Engineering");
        branch.add("Electrical Engineering");

        ArrayAdapter<String> arrayAdapter=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, branch);
        spinner1.setAdapter(arrayAdapter);

        ArrayList<String> Semester=new ArrayList<String>();
        Semester.add("0");
        Semester.add("1");
        Semester.add("2");
        Semester.add("3");
        Semester.add("4");
        Semester.add("5");
        Semester.add("6");
//        String position = (String) spinner1.getItemAtPosition();
//        ArrayList<String>
        ArrayAdapter<String> arrayAdapter1=new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, Semester);
        spinner2.setAdapter(arrayAdapter1);
//        save=findViewById(R.id.Save_Button);
//        spinner2.setPrompt("Select Semester");

    }
    public void click_save(View view){
        String branch = (String)spinner1.getSelectedItem().toString();
        String Semester=(String)spinner2.getSelectedItem().toString();
        if(branch.equals("Computer Engineering")){
            if(Semester.equals("1")){
                Intent intent=new Intent(this, computer_sem_1.class);
                startActivity(intent);
            }
            else if(Semester.equals("Select Semester")){
                Toast.makeText(this, "Please Select Semester", Toast.LENGTH_SHORT).show();
            }
//            Intent intent=new Intent(this, Computer_Engineering.class);
//            startActivity(intent);
        }
        else if(branch.equals("Civil Engineering")){
            Intent intent=new Intent(this, activity_Civil_Engineering.class);
            startActivity(intent);
        }
         else if(branch.equals("Mechanical Engineering")){
            Intent intent=new Intent(this, mechanical_engineering.class);
            startActivity(intent);
        }
         else if(branch.equals("Electrical Engineering")){
            Intent intent=new Intent(this, electrical_engineering.class);
            startActivity(intent);
        }
        else if(branch.equals("Select Branch")){
            Toast.makeText(this, "Invalid Input", Toast.LENGTH_SHORT).show();
        }

    }

}